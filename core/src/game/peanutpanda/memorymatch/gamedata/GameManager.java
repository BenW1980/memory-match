package game.peanutpanda.memorymatch.gamedata;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameManager {

    private User user;
    private Preferences prefs;

    public GameManager() {
        prefs = Gdx.app.getPreferences("game-prefs");
        user = new User();
    }

    public User getUser() {
        return user;
    }
}
