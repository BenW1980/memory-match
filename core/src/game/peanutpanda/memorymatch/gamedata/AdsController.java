package game.peanutpanda.memorymatch.gamedata;

public interface AdsController {

    void showBannerAd();

    void hideBannerAd();
}
