package game.peanutpanda.memorymatch.gamedata;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;

public class AssetLoader {

    private final AssetManager assetManager = new AssetManager();

    public void startLoading() {


        assetManager.load("sounds/victory.ogg", Sound.class);
        assetManager.load("sounds/bloop.ogg", Sound.class);

        assetManager.load("images/title.png", Texture.class);
        assetManager.load("images/info.png", Texture.class);
        assetManager.load("images/play.png", Texture.class);
        assetManager.load("images/credits.png", Texture.class);

        assetManager.load("images/twitter.png", Texture.class);
        assetManager.load("images/twitterOnclick.png", Texture.class);

        assetManager.load("images/animals/1.png", Texture.class);
        assetManager.load("images/animals/2.png", Texture.class);
        assetManager.load("images/animals/3.png", Texture.class);
        assetManager.load("images/animals/4.png", Texture.class);
        assetManager.load("images/animals/5.png", Texture.class);
        assetManager.load("images/animals/6.png", Texture.class);
        assetManager.load("images/animals/7.png", Texture.class);
        assetManager.load("images/animals/8.png", Texture.class);
        assetManager.load("images/animals/9.png", Texture.class);
        assetManager.load("images/animals/10.png", Texture.class);
        assetManager.load("images/animals/emptyWood.png", Texture.class);

        assetManager.load("images/animals/playerSelect.png", Texture.class);
        assetManager.load("images/animals/giraffe.png", Texture.class);
        assetManager.load("images/animals/monkey.png", Texture.class);
        assetManager.load("images/animals/parrot.png", Texture.class);
        assetManager.load("images/animals/snake.png", Texture.class);
        assetManager.load("images/animals/pig.png", Texture.class);
        assetManager.load("images/animals/pigGrey.png", Texture.class);
        assetManager.load("images/animals/penguin.png", Texture.class);
        assetManager.load("images/animals/penguinGrey.png", Texture.class);
        assetManager.load("images/animals/giraffeGrey.png", Texture.class);
        assetManager.load("images/animals/monkeyGrey.png", Texture.class);
        assetManager.load("images/animals/parrotGrey.png", Texture.class);
        assetManager.load("images/animals/snakeGrey.png", Texture.class);
        assetManager.load("images/animals/playerBack.png", Texture.class);
        assetManager.load("images/animals/playerBackOutline.png", Texture.class);
        assetManager.load("images/animals/ps1.png", Texture.class);
        assetManager.load("images/animals/ps2.png", Texture.class);
        assetManager.load("images/animals/ps3.png", Texture.class);
        assetManager.load("images/animals/ps4.png", Texture.class);

        assetManager.load("images/easy.png", Texture.class);
        assetManager.load("images/medium.png", Texture.class);
        assetManager.load("images/hard.png", Texture.class);
        assetManager.load("images/easyChecked.png", Texture.class);
        assetManager.load("images/mediumChecked.png", Texture.class);
        assetManager.load("images/hardChecked.png", Texture.class);

        assetManager.load("images/1playerChecked.png", Texture.class);
        assetManager.load("images/2playersChecked.png", Texture.class);
        assetManager.load("images/3playersChecked.png", Texture.class);
        assetManager.load("images/4playersChecked.png", Texture.class);

        assetManager.load("images/1playerUnchecked.png", Texture.class);
        assetManager.load("images/2playersUnchecked.png", Texture.class);
        assetManager.load("images/3playersUnchecked.png", Texture.class);
        assetManager.load("images/4playersUnchecked.png", Texture.class);

        assetManager.load("images/backgrounds/background1.png", Texture.class);
        assetManager.load("images/backgrounds/background2.png", Texture.class);

        assetManager.load("images/woodBanner.png", Texture.class);

        assetManager.load("images/superduper.png", Texture.class);
        assetManager.load("images/awesome.png", Texture.class);
        assetManager.load("images/nicejob.png", Texture.class);
        assetManager.load("images/welldone.png", Texture.class);

        assetManager.load("images/menuButton.png", Texture.class);
        assetManager.load("images/refreshButton.png", Texture.class);

        assetManager.load("images/stars.png", Texture.class);
        assetManager.load("images/backgroundWhite.png", Texture.class);

        loadFontHandling();

    }

    private void loadFontHandling() {

        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter size10 = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        size10.fontFileName = "fonts/Dimbo_Regular.ttf";
        size10.fontParameters.size = 25;
        assetManager.load("size10.ttf", BitmapFont.class, size10);

    }

    public Sound victory() {
        return assetManager.get("sounds/victory.ogg", Sound.class);
    }

    public Sound bloop() {
        return assetManager.get("sounds/bloop.ogg", Sound.class);
    }

    public Texture title() {
        return assetManager.get("images/title.png", Texture.class);
    }

    public Texture info() {
        return assetManager.get("images/info.png", Texture.class);
    }

    public Texture play() {
        return assetManager.get("images/play.png", Texture.class);
    }

    public Texture credits() {
        return assetManager.get("images/credits.png", Texture.class);
    }

    public Texture twitter() {
        return assetManager.get("images/twitter.png", Texture.class);
    }

    public Texture twitterOnClick() {
        return assetManager.get("images/twitterOnclick.png", Texture.class);
    }

    public Texture onePchecked() {
        return assetManager.get("images/1playerChecked.png", Texture.class);
    }

    public Texture twoPchecked() {
        return assetManager.get("images/2playersChecked.png", Texture.class);
    }

    public Texture threePchecked() {
        return assetManager.get("images/3playersChecked.png", Texture.class);
    }

    public Texture fourPchecked() {
        return assetManager.get("images/4playersChecked.png", Texture.class);
    }

    public Texture onePunchecked() {
        return assetManager.get("images/1playerUnchecked.png", Texture.class);
    }

    public Texture twoPunchecked() {
        return assetManager.get("images/2playersUnchecked.png", Texture.class);
    }

    public Texture threePUnchecked() {
        return assetManager.get("images/3playersUnchecked.png", Texture.class);
    }

    public Texture fourPUnchecked() {
        return assetManager.get("images/4playersUnchecked.png", Texture.class);
    }

    public Texture easy() {
        return assetManager.get("images/easy.png", Texture.class);
    }

    public Texture medium() {
        return assetManager.get("images/medium.png", Texture.class);
    }

    public Texture hard() {
        return assetManager.get("images/hard.png", Texture.class);
    }

    public Texture easyChecked() {
        return assetManager.get("images/easyChecked.png", Texture.class);
    }

    public Texture mediumChecked() {
        return assetManager.get("images/mediumChecked.png", Texture.class);
    }

    public Texture hardChecked() {
        return assetManager.get("images/hardChecked.png", Texture.class);
    }

    public Texture animal1() {
        return assetManager.get("images/animals/1.png", Texture.class);
    }

    public Texture animal2() {
        return assetManager.get("images/animals/2.png", Texture.class);
    }

    public Texture animal3() {
        return assetManager.get("images/animals/3.png", Texture.class);
    }

    public Texture animal4() {
        return assetManager.get("images/animals/4.png", Texture.class);
    }

    public Texture animal5() {
        return assetManager.get("images/animals/5.png", Texture.class);
    }

    public Texture animal6() {
        return assetManager.get("images/animals/6.png", Texture.class);
    }

    public Texture animal7() {
        return assetManager.get("images/animals/7.png", Texture.class);
    }

    public Texture animal8() {
        return assetManager.get("images/animals/8.png", Texture.class);
    }

    public Texture animal9() {
        return assetManager.get("images/animals/9.png", Texture.class);
    }

    public Texture animal10() {
        return assetManager.get("images/animals/10.png", Texture.class);
    }

    public Texture emptyWood() {
        return assetManager.get("images/animals/emptyWood.png", Texture.class);
    }

    public Texture pig() {
        return assetManager.get("images/animals/pig.png", Texture.class);
    }

    public Texture pigGrey() {
        return assetManager.get("images/animals/pigGrey.png", Texture.class);
    }

    public Texture penguin() {
        return assetManager.get("images/animals/penguin.png", Texture.class);
    }

    public Texture penguinGrey() {
        return assetManager.get("images/animals/penguinGrey.png", Texture.class);
    }

    public Texture giraffe() {
        return assetManager.get("images/animals/giraffe.png", Texture.class);
    }

    public Texture monkey() {
        return assetManager.get("images/animals/monkey.png", Texture.class);
    }

    public Texture parrot() {
        return assetManager.get("images/animals/parrot.png", Texture.class);
    }

    public Texture snake() {
        return assetManager.get("images/animals/snake.png", Texture.class);
    }

    public Texture giraffeGrey() {
        return assetManager.get("images/animals/giraffeGrey.png", Texture.class);
    }

    public Texture monkeyGrey() {
        return assetManager.get("images/animals/monkeyGrey.png", Texture.class);
    }

    public Texture parrotGrey() {
        return assetManager.get("images/animals/parrotGrey.png", Texture.class);
    }

    public Texture snakeGrey() {
        return assetManager.get("images/animals/snakeGrey.png", Texture.class);
    }

    public Texture playerSelect() {
        return assetManager.get("images/animals/playerSelect.png", Texture.class);
    }

    public Texture ps1() {
        return assetManager.get("images/animals/ps1.png", Texture.class);
    }

    public Texture ps2() {
        return assetManager.get("images/animals/ps2.png", Texture.class);
    }

    public Texture ps3() {
        return assetManager.get("images/animals/ps3.png", Texture.class);
    }

    public Texture ps4() {
        return assetManager.get("images/animals/ps4.png", Texture.class);
    }

    public Texture playerBack() {
        return assetManager.get("images/animals/playerBack.png", Texture.class);
    }

    public Texture playerBackOutline() {
        return assetManager.get("images/animals/playerBackOutline.png", Texture.class);
    }

    public Texture background1() {
        return assetManager.get("images/backgrounds/background1.png", Texture.class);
    }

    public Texture background2() {
        return assetManager.get("images/backgrounds/background2.png", Texture.class);
    }

    public Texture woodBanner() {
        return assetManager.get("images/woodBanner.png", Texture.class);
    }

    public Texture superduper() {
        return assetManager.get("images/superduper.png", Texture.class);
    }

    public Texture awesome() {
        return assetManager.get("images/awesome.png", Texture.class);
    }

    public Texture nicejob() {
        return assetManager.get("images/nicejob.png", Texture.class);
    }

    public Texture welldone() {
        return assetManager.get("images/welldone.png", Texture.class);
    }

    public Texture menuButton() {
        return assetManager.get("images/menuButton.png", Texture.class);
    }

    public Texture refreshButton() {
        return assetManager.get("images/refreshButton.png", Texture.class);
    }

    public Texture stars() {
        return assetManager.get("images/stars.png", Texture.class);
    }

    public Texture backgroundWhite() {
        return assetManager.get("images/backgroundWhite.png", Texture.class);
    }


    public AssetManager getAssetManager() {
        return assetManager;
    }
}
