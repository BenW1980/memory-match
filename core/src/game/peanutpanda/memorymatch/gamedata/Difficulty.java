package game.peanutpanda.memorymatch.gamedata;

public enum Difficulty {

    EASY, MEDIUM, HARD, ADVANCED

}
