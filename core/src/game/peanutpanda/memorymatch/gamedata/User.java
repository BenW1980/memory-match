package game.peanutpanda.memorymatch.gamedata;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class User {

    private Rectangle rectangle;

    public User() {
        rectangle = new Rectangle(0, 0, 1, 1);
    }

    public boolean taps(Rectangle rect) {

        if (this.rectangle == null) {
            rectangle = new Rectangle(0, 0, 1, 1);
        }
        return this.rectangle.overlaps(rect);
    }

    public void move(Vector3 worldCoordinates) {

        rectangle.y = worldCoordinates.y;
        rectangle.x = worldCoordinates.x;
    }
}
