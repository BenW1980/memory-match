package game.peanutpanda.memorymatch.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.memorymatch.gamedata.AssetLoader;
import game.peanutpanda.memorymatch.gamedata.AudioHandler;
import game.peanutpanda.memorymatch.gamedata.Difficulty;
import game.peanutpanda.memorymatch.gamedata.GameManager;
import game.peanutpanda.memorymatch.gamedata.MenuState;
import game.peanutpanda.memorymatch.gamedata.ScreenSize;
import game.peanutpanda.memorymatch.gamedata.User;
import game.peanutpanda.memorymatch.gameobjects.PlayerNumber;
import game.peanutpanda.memorymatch.gameobjects.PlayerSprite;

public class MainMenuScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private User user;
    private GameManager gMan;
    private AudioHandler audioHandler;
    private AssetLoader assetLoader;
    private FitViewport viewport;

    private Difficulty difficulty;

    private boolean onePlayerChecked = true;
    private boolean twoPlayerChecked = false;
    private boolean threePlayerChecked = false;
    private boolean fourPlayerChecked = false;

    private boolean easyChecked = false;
    private boolean mediumChecked = true;
    private boolean hardChecked = false;

    private PlayerNumber playerNumber = PlayerNumber.ONE;

    private Rectangle easy;
    private Rectangle medium;
    private Rectangle hard;

    private Rectangle checkBoxp1;
    private Rectangle checkBoxp2;
    private Rectangle checkBoxp3;
    private Rectangle checkBoxp4;

    private Rectangle giraffeRect;
    private Rectangle monkeyRect;
    private Rectangle parrotRect;
    private Rectangle snakeRect;
    private Rectangle pigRect;
    private Rectangle penguinRect;

    private Rectangle playOnMenuRect;
    private Rectangle playOnPSRect;
    private Rectangle infoRect;

    private Texture p1Texture;
    private Texture p2Texture;
    private Texture p3Texture;
    private Texture p4Texture;

    private Texture easyTexture;
    private Texture mediumTexture;
    private Texture hardTexture;

    private PlayerSprite giraffeSprite;
    private PlayerSprite monkeySprite;
    private PlayerSprite parrotSprite;
    private PlayerSprite snakeSprite;
    private PlayerSprite pigSprite;
    private PlayerSprite penguinSprite;

    private Sprite giraffeLowerSprite;
    private Sprite monkeyLowerSprite;
    private Sprite parrotLowerSprite;
    private Sprite snakeLowerSprite;
    private Sprite pigLowerSprite;
    private Sprite penguinLowerSprite;

    private Array<PlayerSprite> playerAnimalSprites;
    private Array<PlayerSprite> chosenPlayerSprites;


    private int easyTouch = 0;
    private int mediumTouch = 0;
    private int hardTouch = 0;

    private int p1Touch = 0;
    private int p2Touch = 0;
    private int p3Touch = 0;
    private int p4Touch = 0;

    private int playTouch = 0;
    private int infoTouch = 0;

    private MenuState menuState;

    private int playerRectWidth = 120;
    private int playerRectHeight = 120;
    private int rectWidth = 180;
    private int rectHeight = 60;

    private boolean playersChosen = false;

    public MainMenuScreen(SpriteBatch batch, OrthographicCamera camera, User user, GameManager gMan, AssetLoader assetLoader) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.batch = batch;
        this.camera = camera;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.user = user;
        this.gMan = gMan;
        this.assetLoader = assetLoader;
        audioHandler = new AudioHandler(assetLoader);
        audioHandler.playMusic();
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        p1Texture = assetLoader.onePunchecked();
        p2Texture = assetLoader.twoPunchecked();
        p3Texture = assetLoader.threePUnchecked();
        p4Texture = assetLoader.fourPUnchecked();

        easyTexture = assetLoader.easy();
        mediumTexture = assetLoader.medium();
        hardTexture = assetLoader.hard();

        difficulty = Difficulty.MEDIUM;
        menuState = MenuState.MENU;

        playerAnimalSprites = new Array<PlayerSprite>();
        chosenPlayerSprites = new Array<PlayerSprite>();

        easy = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 100, 550, 200, 61);
        medium = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 100, 480, 200, 61);
        hard = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 100, 410, 200, 61);

        checkBoxp1 = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 180, 330, rectWidth, rectHeight);
        checkBoxp2 = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 5, 330, rectWidth, rectHeight);
        checkBoxp3 = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 180, 265, rectWidth, rectHeight);
        checkBoxp4 = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 5, 265, rectWidth, rectHeight);

        playOnMenuRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 20, 110, 94, 120);
        infoRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 114, 110, 94, 120);
        playOnPSRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 48, 200, 94, 120);

        giraffeRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 210, ScreenSize.HEIGHT.getSize() / 2 + 85, playerRectWidth, playerRectHeight);
        monkeyRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 210, ScreenSize.HEIGHT.getSize() / 2 - 50, playerRectWidth, playerRectHeight);
        parrotRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 65, ScreenSize.HEIGHT.getSize() / 2 + 85, playerRectWidth, playerRectHeight);
        snakeRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 65, ScreenSize.HEIGHT.getSize() / 2 - 50, playerRectWidth, playerRectHeight);

        pigRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 80, ScreenSize.HEIGHT.getSize() / 2 + 87, playerRectWidth - 5, playerRectHeight - 5);
        penguinRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 80, ScreenSize.HEIGHT.getSize() / 2 - 45, playerRectWidth - 5, playerRectHeight - 5);

        giraffeSprite = new PlayerSprite(assetLoader.giraffe(), giraffeRect, false);
        monkeySprite = new PlayerSprite(assetLoader.monkey(), monkeyRect, false);
        parrotSprite = new PlayerSprite(assetLoader.parrot(), parrotRect, false);
        snakeSprite = new PlayerSprite(assetLoader.snake(), snakeRect, false);
        pigSprite = new PlayerSprite(assetLoader.pig(), pigRect, false);
        penguinSprite = new PlayerSprite(assetLoader.penguin(), penguinRect, false);

        playerAnimalSprites.add(giraffeSprite);
        playerAnimalSprites.add(monkeySprite);
        playerAnimalSprites.add(parrotSprite);
        playerAnimalSprites.add(snakeSprite);
        playerAnimalSprites.add(pigSprite);
        playerAnimalSprites.add(penguinSprite);

        giraffeLowerSprite = new Sprite(assetLoader.giraffeGrey());
        monkeyLowerSprite = new Sprite(assetLoader.monkeyGrey());
        parrotLowerSprite = new Sprite(assetLoader.parrotGrey());
        snakeLowerSprite = new Sprite(assetLoader.snakeGrey());
        pigLowerSprite = new Sprite(assetLoader.pigGrey());
        penguinLowerSprite = new Sprite(assetLoader.penguinGrey());

        giraffeLowerSprite.setBounds(giraffeRect.x, giraffeRect.y, giraffeRect.width, giraffeRect.height);
        monkeyLowerSprite.setBounds(monkeyRect.x, monkeyRect.y, monkeyRect.width, monkeyRect.height);
        parrotLowerSprite.setBounds(parrotRect.x, parrotRect.y, parrotRect.width, parrotRect.height);
        snakeLowerSprite.setBounds(snakeRect.x, snakeRect.y, snakeRect.width, snakeRect.height);
        pigLowerSprite.setBounds(pigRect.x, pigRect.y, pigRect.width, pigRect.height);
        penguinLowerSprite.setBounds(penguinRect.x, penguinRect.y, penguinRect.width, penguinRect.height);

    }

    @Override
    public void show() {

    }

    private void drawMenu() {

        batch.draw(assetLoader.title(), 30, 680, 420, 90);

        batch.draw(easyTexture, easy.x, easy.y, easy.width - easyTouch, easy.height - easyTouch);
        batch.draw(mediumTexture, medium.x, medium.y, medium.width - mediumTouch, medium.height - mediumTouch);
        batch.draw(hardTexture, hard.x, hard.y, hard.width - hardTouch, hard.height - hardTouch);

        batch.draw(p1Texture, checkBoxp1.x, checkBoxp1.y, checkBoxp1.width - p1Touch, checkBoxp1.height - p1Touch);
        batch.draw(p2Texture, checkBoxp2.x, checkBoxp2.y, checkBoxp2.width - p2Touch, checkBoxp2.height - p2Touch);
        batch.draw(p3Texture, checkBoxp3.x, checkBoxp3.y, checkBoxp3.width - p3Touch, checkBoxp3.height - p3Touch);
        batch.draw(p4Texture, checkBoxp4.x, checkBoxp4.y, checkBoxp4.width - p4Touch, checkBoxp4.height - p4Touch);

        batch.draw(assetLoader.play(), playOnMenuRect.x, playOnMenuRect.y, playOnMenuRect.width - playTouch, playOnMenuRect.height - playTouch);
        batch.draw(assetLoader.info(), infoRect.x, infoRect.y, infoRect.width - infoTouch, infoRect.height - infoTouch);

        if (easyChecked) {
            easyTexture = assetLoader.easyChecked();
            mediumTexture = assetLoader.medium();
            hardTexture = assetLoader.hard();

        } else if (mediumChecked) {
            easyTexture = assetLoader.easy();
            mediumTexture = assetLoader.mediumChecked();
            hardTexture = assetLoader.hard();

        } else if (hardChecked) {
            easyTexture = assetLoader.easy();
            mediumTexture = assetLoader.medium();
            hardTexture = assetLoader.hardChecked();
        }

        if (onePlayerChecked) {
            p1Texture = assetLoader.onePchecked();
            p2Texture = assetLoader.twoPunchecked();
            p3Texture = assetLoader.threePUnchecked();
            p4Texture = assetLoader.fourPUnchecked();

        } else if (twoPlayerChecked) {
            p1Texture = assetLoader.onePunchecked();
            p2Texture = assetLoader.twoPchecked();
            p3Texture = assetLoader.threePUnchecked();
            p4Texture = assetLoader.fourPUnchecked();

        } else if (threePlayerChecked) {
            p1Texture = assetLoader.onePunchecked();
            p2Texture = assetLoader.twoPunchecked();
            p3Texture = assetLoader.threePchecked();
            p4Texture = assetLoader.fourPUnchecked();

        } else if (fourPlayerChecked) {
            p1Texture = assetLoader.onePunchecked();
            p2Texture = assetLoader.twoPunchecked();
            p3Texture = assetLoader.threePUnchecked();
            p4Texture = assetLoader.fourPchecked();
        }
    }

    private void renderPlayButtonPS() {

        switch (playerNumber) {
            case TWO:
                if (chosenPlayerSprites.size == 2) {
                    batch.draw(assetLoader.play(), playOnPSRect.x, playOnPSRect.y, playOnPSRect.width, playOnPSRect.height);
                    playersChosen = true;
                }
                break;
            case THREE:
                if (chosenPlayerSprites.size == 3) {
                    batch.draw(assetLoader.play(), playOnPSRect.x, playOnPSRect.y, playOnPSRect.width, playOnPSRect.height);
                    playersChosen = true;
                }
                break;
            case FOUR:
                if (chosenPlayerSprites.size == 4) {
                    batch.draw(assetLoader.play(), playOnPSRect.x, playOnPSRect.y, playOnPSRect.width, playOnPSRect.height);
                    playersChosen = true;
                }
                break;
        }
    }

    private void drawPlayerSelect() {

        int width = 70;
        int height = 70;
        int y = 100;
        int size = 70;

        batch.draw(assetLoader.playerSelect(), ScreenSize.WIDTH.getSize() / 2 - 225, 700, 450, 60);

        switch (playerNumber) {
            case TWO:
                if (chosenPlayerSprites.size == 0) {
                    batch.draw(assetLoader.ps1(), ScreenSize.WIDTH.getSize() / 2 - 85, y, width, height);
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 + 15, y, width, height);
                } else if (chosenPlayerSprites.size == 1) {
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 + 15, y, width, height);

                }
                break;
            case THREE:
                if (chosenPlayerSprites.size == 0) {
                    batch.draw(assetLoader.ps1(), ScreenSize.WIDTH.getSize() / 2 - 135, y, width, height);
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 - 35, y, width, height);
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 65, y, width, height);

                } else if (chosenPlayerSprites.size == 1) {
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 - 35, y, width, height);
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 65, y, width, height);

                } else if (chosenPlayerSprites.size == 2) {
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 65, y, width, height);
                }

                break;
            case FOUR:
                if (chosenPlayerSprites.size == 0) {
                    batch.draw(assetLoader.ps1(), ScreenSize.WIDTH.getSize() / 2 - 185, y, width, height);
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 - 85, y, width, height);
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 15, y, width, height);
                    batch.draw(assetLoader.ps4(), ScreenSize.WIDTH.getSize() / 2 + 115, y, width, height);
                } else if (chosenPlayerSprites.size == 1) {
                    batch.draw(assetLoader.ps2(), ScreenSize.WIDTH.getSize() / 2 - 85, y, width, height);
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 15, y, width, height);
                    batch.draw(assetLoader.ps4(), ScreenSize.WIDTH.getSize() / 2 + 115, y, width, height);

                } else if (chosenPlayerSprites.size == 2) {
                    batch.draw(assetLoader.ps3(), ScreenSize.WIDTH.getSize() / 2 + 15, y, width, height);
                    batch.draw(assetLoader.ps4(), ScreenSize.WIDTH.getSize() / 2 + 115, y, width, height);

                } else if (chosenPlayerSprites.size == 3) {
                    batch.draw(assetLoader.ps4(), ScreenSize.WIDTH.getSize() / 2 + 115, y, width, height);
                }
                break;
        }

        giraffeLowerSprite.draw(batch);
        monkeyLowerSprite.draw(batch);
        parrotLowerSprite.draw(batch);
        snakeLowerSprite.draw(batch);
        pigLowerSprite.draw(batch);
        penguinLowerSprite.draw(batch);

        if (!giraffeSprite.isChosen()) {
            giraffeSprite.setRect(giraffeRect);
            giraffeSprite.getSprite().draw(batch);
        }
        if (!monkeySprite.isChosen()) {
            monkeySprite.setRect(monkeyRect);
            monkeySprite.getSprite().draw(batch);
        }
        if (!parrotSprite.isChosen()) {
            parrotSprite.setRect(parrotRect);
            parrotSprite.getSprite().draw(batch);
        }
        if (!snakeSprite.isChosen()) {
            snakeSprite.setRect(snakeRect);
            snakeSprite.getSprite().draw(batch);
        }
        if (!pigSprite.isChosen()) {
            pigSprite.setRect(pigRect);
            pigSprite.getSprite().draw(batch);
        }
        if (!penguinSprite.isChosen()) {
            penguinSprite.setRect(penguinRect);
            penguinSprite.getSprite().draw(batch);
        }

        for (int i = 0; i < chosenPlayerSprites.size; i++) {
            if (chosenPlayerSprites.get(i).isChosen()) {
                if (playerNumber == PlayerNumber.TWO) {
                    chosenPlayerSprites.get(i).setRect(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 85 + (i * 100), y, size, size));
                } else if (playerNumber == PlayerNumber.THREE) {
                    chosenPlayerSprites.get(i).setRect(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 135 + (i * 100), y, size, size));
                } else if (playerNumber == PlayerNumber.FOUR) {
                    chosenPlayerSprites.get(i).setRect(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 185 - 3 + (i * 100), y - 2, size, size));
                }
            }
            batch.draw(chosenPlayerSprites.get(i).getSprite(), chosenPlayerSprites.get(i).getRect().x, chosenPlayerSprites.get(i).getRect().y, size, size);
        }
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        batch.draw(assetLoader.background2(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        batch.draw(assetLoader.woodBanner(), 0, 0, ScreenSize.WIDTH.getSize(), 80);

        if (menuState == MenuState.MENU) {
            drawMenu();
        } else {
            drawPlayerSelect();
            renderPlayButtonPS();
        }

        batch.end();

    }

    private void touchDownButtonsMenu() {

        if (user.taps(checkBoxp1)) {
            p1Touch = 5;
            onePlayerChecked = true;
            twoPlayerChecked = false;
            threePlayerChecked = false;
            fourPlayerChecked = false;
            playerNumber = PlayerNumber.ONE;
        } else {
            p1Touch = 0;
        }

        if (user.taps(checkBoxp2)) {
            p2Touch = 5;
            onePlayerChecked = false;
            twoPlayerChecked = true;
            threePlayerChecked = false;
            fourPlayerChecked = false;
            playerNumber = PlayerNumber.TWO;
        } else {
            p2Touch = 0;
        }

        if (user.taps(checkBoxp3)) {
            p3Touch = 5;
            onePlayerChecked = false;
            twoPlayerChecked = false;
            threePlayerChecked = true;
            fourPlayerChecked = false;
            playerNumber = PlayerNumber.THREE;
        } else {
            p3Touch = 0;
        }

        if (user.taps(checkBoxp4)) {
            p4Touch = 5;
            onePlayerChecked = false;
            twoPlayerChecked = false;
            threePlayerChecked = false;
            fourPlayerChecked = true;
            playerNumber = PlayerNumber.FOUR;
        } else {
            p4Touch = 0;
        }

        if (user.taps(easy)) {
            hardTouch = 0;
            mediumTouch = 0;
            easyTouch = 5;
            easyChecked = true;
            mediumChecked = false;
            hardChecked = false;
            difficulty = Difficulty.EASY;

        } else if (user.taps(medium)) {
            easyTouch = 0;
            hardTouch = 0;
            mediumTouch = 5;
            easyChecked = false;
            mediumChecked = true;
            hardChecked = false;
            difficulty = Difficulty.MEDIUM;

        } else if (user.taps(hard)) {
            easyTouch = 0;
            mediumTouch = 0;
            hardTouch = 5;
            easyChecked = false;
            mediumChecked = false;
            hardChecked = true;
            difficulty = Difficulty.HARD;
        }

        if (user.taps(playOnMenuRect)) {
            playTouch = 5;
        } else {
            playTouch = 0;
        }
        if (user.taps(infoRect)) {
            infoTouch = 5;
        } else {
            infoTouch = 0;
        }
    }

    private void selectPlayers() {

        for (PlayerSprite ps : playerAnimalSprites) {
            if (user.taps(ps.getRect())) {
                if (!chosenPlayerSprites.contains(ps, true)) {
                    addChosenPlayers(ps);

                } else {
                    ps.setChosen(false);
                    chosenPlayerSprites.removeValue(ps, true);
                }
            }
        }
    }

    private void addChosenPlayers(PlayerSprite ps) {

        switch (playerNumber) {
            case TWO:
                if (chosenPlayerSprites.size < 2) {
                    ps.setChosen(true);
                    chosenPlayerSprites.add(ps);
                }
                break;
            case THREE:
                if (chosenPlayerSprites.size < 3) {
                    ps.setChosen(true);
                    chosenPlayerSprites.add(ps);
                }
                break;
            case FOUR:
                if (chosenPlayerSprites.size < 4) {
                    ps.setChosen(true);
                    chosenPlayerSprites.add(ps);
                }
                break;
        }

    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        if (menuState == MenuState.MENU) {
            touchDownButtonsMenu();
        } else {
            selectPlayers();
            if (playersChosen && user.taps(playOnPSRect)) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(batch, camera, user, gMan, assetLoader, difficulty, playerNumber, chosenPlayerSprites));
            }
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        easyTouch = 0;
        mediumTouch = 0;
        hardTouch = 0;

        p1Touch = 0;
        p2Touch = 0;
        p3Touch = 0;
        p4Touch = 0;

        playTouch = 0;
        infoTouch = 0;

        if (menuState == MenuState.MENU) {
            if (user.taps(playOnMenuRect)) {
                if (playerNumber != PlayerNumber.ONE) {
                    menuState = MenuState.PLAYER_SELECT;
                } else {
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(batch, camera, user, gMan, assetLoader, difficulty, playerNumber, chosenPlayerSprites));
                }
            } else if (user.taps(infoRect)) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new CreditsScreen(camera, batch, user, gMan, assetLoader));
            }

        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        if (menuState == MenuState.MENU) {
            touchDownButtonsMenu();
        }

        return true;

    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            if (menuState == MenuState.PLAYER_SELECT) {
                menuState = MenuState.MENU;
                for (PlayerSprite ps : playerAnimalSprites) {
                    ps.setChosen(false);
                    chosenPlayerSprites = new Array<PlayerSprite>();
                }
            } else {
                Gdx.app.exit();
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();
    }
}