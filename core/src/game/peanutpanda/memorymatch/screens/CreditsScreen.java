package game.peanutpanda.memorymatch.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.memorymatch.gamedata.AssetLoader;
import game.peanutpanda.memorymatch.gamedata.GameManager;
import game.peanutpanda.memorymatch.gamedata.ScreenSize;
import game.peanutpanda.memorymatch.gamedata.User;


public class CreditsScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private GameManager gMan;
    private User user;
    private Vector3 worldCoordinates;
    private AssetLoader assetLoader;
    private FitViewport viewport;

    private Texture backToMain;
    private Rectangle backToMainRect;
    private Rectangle twRectPP;
    private Rectangle twRectEW;

    private Texture twButtonTexturePP;
    private Texture twButtonTextureEW;

    private int backToMainTouch = 0;

    public CreditsScreen(OrthographicCamera camera, SpriteBatch batch, User user, GameManager gMan, AssetLoader assetLoader) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.camera = camera;
        this.batch = batch;
        this.user = user;
        this.gMan = gMan;
        this.assetLoader = assetLoader;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);

        backToMainRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 80, 120, 160, 109);
        backToMain = assetLoader.menuButton();

        twRectPP = new Rectangle(337, 673, 35, 30);
        twRectEW = new Rectangle(337, 549, 35, 30);
        twButtonTexturePP = assetLoader.twitter();
        twButtonTextureEW = assetLoader.twitter();

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        batch.draw(assetLoader.background2(), 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        batch.draw(assetLoader.credits(), ScreenSize.WIDTH.getSize() / 2 - 211, ScreenSize.HEIGHT.getSize() / 2 - 100, 422, 466);

        batch.draw(backToMain, backToMainRect.x, backToMainRect.y, backToMainRect.width - backToMainTouch, backToMainRect.height - backToMainTouch);

        batch.draw(twButtonTexturePP, twRectPP.x, twRectPP.y, twRectPP.width, twRectPP.height);
        batch.draw(twButtonTextureEW, twRectEW.x, twRectEW.y, twRectEW.width, twRectEW.height);
        batch.end();

    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, user, gMan, assetLoader));
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        if (user.taps(backToMainRect)) {
            backToMainTouch = 5;
        }

        if (user.taps(twRectPP)) {
            twButtonTexturePP = assetLoader.twitterOnClick();
        } else {
            twButtonTexturePP = assetLoader.twitter();
        }

        if (user.taps(twRectEW)) {
            twButtonTextureEW = assetLoader.twitterOnClick();
        } else {
            twButtonTextureEW = assetLoader.twitter();
        }


        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        backToMainTouch = 0;

        if (user.taps(backToMainRect)) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, user, gMan, assetLoader));
        }

        if (user.taps(twRectPP)) {
            twButtonTexturePP = assetLoader.twitter();
            if (!Gdx.net.openURI("twitter://user?user_id=741271262006251520")) { // open app
                Gdx.net.openURI("https://twitter.com/PeanutPandaApps"); // open site
            }
        }

        if (user.taps(twRectEW)) {
            twButtonTextureEW = assetLoader.twitter();
            if (!Gdx.net.openURI("twitter://user?user_id=753832720002789377")) { // open app
                Gdx.net.openURI("https://twitter.com/eva_willaert"); // open site
            }
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();

    }
}
