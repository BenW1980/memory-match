package game.peanutpanda.memorymatch.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.memorymatch.gamedata.AssetLoader;
import game.peanutpanda.memorymatch.gamedata.Difficulty;
import game.peanutpanda.memorymatch.gamedata.GameManager;
import game.peanutpanda.memorymatch.gamedata.ScreenSize;
import game.peanutpanda.memorymatch.gamedata.User;
import game.peanutpanda.memorymatch.gameobjects.Card;
import game.peanutpanda.memorymatch.gameobjects.CardManager;
import game.peanutpanda.memorymatch.gameobjects.Player;
import game.peanutpanda.memorymatch.gameobjects.PlayerNumber;
import game.peanutpanda.memorymatch.gameobjects.PlayerSprite;

public class GameScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private User user;
    private GameManager gMan;
    private AssetLoader assetLoader;
    private CardManager cMan;
    private Difficulty difficulty;
    private Array<Card> cardArray;
    private Array<Rectangle> rectangleArray;
    private Array<Card> chosenCards;
    private float timePassed = 0;
    private Sprite cardBackground;
    private PlayerNumber playerNumber;
    private Player player1;
    private Player player2;
    private Player player3;
    private Player player4;
    private Player activePlayer;
    private int width;
    private int height;
    private int uncoverSpeed;
    private Texture background;
    private FitViewport viewport;
    private boolean cardsDropped;
    private Array<PlayerSprite> chosenPlayerSprites;
    private BitmapFont font;
    private Rectangle outlineRect;
    private Sprite outlineSprite;
    private Array<Player> playerArray;
    private int cardsLeft;

    private int player1Y = 715;
    private int player2Y = 715;
    private int player3Y = 715;
    private int player4Y = 715;

    private boolean player1Jumped = false;
    private boolean player2Jumped = false;
    private boolean player3Jumped = false;
    private boolean player4Jumped = false;

    private int jumpSpeed = 300;
    private int jumpCeiling = 740;

    public GameScreen(SpriteBatch batch, OrthographicCamera camera, User user, GameManager gMan, AssetLoader assetLoader, Difficulty difficulty, PlayerNumber playerNumber, Array<PlayerSprite> chosenPlayerSprites) {
        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.camera = camera;
        this.batch = batch;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.user = user;
        this.gMan = gMan;
        this.assetLoader = assetLoader;
        this.font = assetLoader.getAssetManager().get("size10.ttf", BitmapFont.class);
        this.difficulty = difficulty;
        this.playerNumber = playerNumber;
        cardBackground = new Sprite(assetLoader.emptyWood());
        cMan = new CardManager(assetLoader);
        cMan.resetArrays();
        cMan.fillTotalRegularCardsArray();
        cMan.generateCards(difficulty);
        cardArray = setCardArray(difficulty);
        rectangleArray = setRectangleArray(difficulty);
        chosenCards = new Array<Card>();
        initPlayers();
        activePlayer = player1;
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        this.chosenPlayerSprites = chosenPlayerSprites;
        this.outlineRect = new Rectangle(40 - 8, 715 - 30, 90, 110);
        this.outlineSprite = new Sprite(assetLoader.playerBackOutline());
        outlineSprite.setBounds(outlineRect.x, outlineRect.y, outlineRect.width, outlineRect.height);
        playerArray = new Array<Player>();
        cardsLeft = cardArray.size;

        switch (playerNumber) {
            case TWO:
                playerArray.add(player1);
                playerArray.add(player2);
                break;
            case THREE:
                playerArray.add(player1);
                playerArray.add(player2);
                playerArray.add(player3);
                break;
            case FOUR:
                playerArray.add(player1);
                playerArray.add(player2);
                playerArray.add(player3);
                playerArray.add(player4);
                break;
        }

        if (difficulty == Difficulty.EASY) {
            width = cMan.getEasyWidth();
            height = cMan.getEasyHeight();
            uncoverSpeed = 1000;

        } else if (difficulty == Difficulty.MEDIUM) {
            width = cMan.getMediumWidth();
            height = cMan.getMediumHeight();
            uncoverSpeed = 800;

        } else if (difficulty == Difficulty.HARD) {
            width = cMan.getHardWidth();
            height = cMan.getHardHeight();
            uncoverSpeed = 600;
        }

        for (Card card : cardArray) {
            card.setBackgroundHeight(-height);
        }

        background = randomBackground();

    }

    private Texture randomBackground() {

        if (MathUtils.random(1, 2) == 1) {
            return assetLoader.background1();
        }

        return assetLoader.background2();
    }

    private void initPlayers() {

        player1 = new Player();

        if (playerNumber == PlayerNumber.TWO) {
            player2 = new Player();
        }

        if (playerNumber == PlayerNumber.THREE) {
            player2 = new Player();
            player3 = new Player();
        }

        if (playerNumber == PlayerNumber.FOUR) {
            player2 = new Player();
            player3 = new Player();
            player4 = new Player();
        }
    }

    private Array<Card> setCardArray(Difficulty difficulty) {

        switch (difficulty) {

            case EASY:
                cardArray = cMan.getEasyCardsArray();
                break;
            case MEDIUM:
                cardArray = cMan.getMediumCardsArray();
                break;
            case HARD:
                cardArray = cMan.getHardCardsArray();
                break;
        }

        return cardArray;
    }

    private Array<Rectangle> setRectangleArray(Difficulty difficulty) {

        switch (difficulty) {

            case EASY:
                rectangleArray = cMan.getCardRectanglesEasy();
                break;
            case MEDIUM:
                rectangleArray = cMan.getCardRectanglesMedium();
                break;
            case HARD:
                rectangleArray = cMan.getCardRectanglesHard();
                break;
        }

        return rectangleArray;
    }

    private Player nextPlayer() {

        Player player = null;

        if (playerNumber == PlayerNumber.ONE) {
            player = activePlayer;

        } else if (playerNumber == PlayerNumber.TWO) {

            if (activePlayer == player1) {
                player = player2;
            } else if (activePlayer == player2) {
                player = player1;
            }

        } else if (playerNumber == PlayerNumber.THREE) {

            if (activePlayer == player1) {
                player = player2;
            } else if (activePlayer == player2) {
                player = player3;
            } else if (activePlayer == player3) {
                player = player1;
            }

        } else if (playerNumber == PlayerNumber.FOUR) {

            if (activePlayer == player1) {
                player = player2;
            } else if (activePlayer == player2) {
                player = player3;
            } else if (activePlayer == player3) {
                player = player4;
            } else if (activePlayer == player4) {
                player = player1;
            }
        }

        return player;
    }

    private int totalScore() {

        int result = 0;

        for (Player player : playerArray) {
            result += player.getScore();
        }

        return result;
    }

    private void setJumpedFalse() {
        player1Jumped = false;
        player2Jumped = false;
        player3Jumped = false;
        player4Jumped = false;
    }

    private void checkIfEqual(float delta) {

        if (chosenCards.size == 2) {
            timePassed += delta;
            if (chosenCards.get(0).getNumber() == chosenCards.get(1).getNumber() && timePassed > 0.7f) {
                chosenCards.get(0).setSolved(true);
                chosenCards.get(1).setSolved(true);
                activePlayer.increaseScore();
                chosenCards.removeRange(0, 1);
                cardsLeft -= 2;
                assetLoader.bloop().play();
                timePassed = 0;
                setJumpedFalse();

            } else {

                if (timePassed > 1f) {
                    activePlayer = nextPlayer();
                    for (Card card : chosenCards) {
                        card.setChosen(false);
                        card.setUp(true);
                    }
                    chosenCards.removeRange(0, 1);
                    timePassed = 0;
                }
            }
        }
    }

    private void renderPlayerSprites() {

        int width = 75;
        int height = 75;
        int backWidth = 90;
        int backHeight = 110;
        int y = 715;

        switch (playerNumber) {
            case TWO:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, player1Y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, player2Y, width, height);
                font.draw(batch, "" + player1.getScore(), 74, y - 5);
                font.draw(batch, "" + player2.getScore(), 179, y - 5);
                break;
            case THREE:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, player1Y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, player2Y, width, height);
                chosenPlayerSprites.get(2).getSprite().setBounds(250, player3Y, width, height);
                font.draw(batch, "" + player1.getScore(), 74, y - 5);
                font.draw(batch, "" + player2.getScore(), 179, y - 5);
                font.draw(batch, "" + player3.getScore(), 284, y - 5);
                break;
            case FOUR:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, player1Y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, player2Y, width, height);
                chosenPlayerSprites.get(2).getSprite().setBounds(250, player3Y, width, height);
                chosenPlayerSprites.get(3).getSprite().setBounds(355, player4Y, width, height);
                font.draw(batch, "" + player1.getScore(), 74, y - 5);
                font.draw(batch, "" + player2.getScore(), 179, y - 5);
                font.draw(batch, "" + player3.getScore(), 284, y - 5);
                font.draw(batch, "" + player4.getScore(), 389, y - 5);
                break;
        }

        for (int i = 0; i < chosenPlayerSprites.size; i++) {
            chosenPlayerSprites.get(i).getSprite().draw(batch);
        }

        if (activePlayer == player1) {
            outlineSprite.setBounds(40 - 8, y - 30, backWidth, backHeight);
        } else if (activePlayer == player2) {
            outlineSprite.setBounds(145 - 8, y - 30, backWidth, backHeight);
        } else if (activePlayer == player3) {
            outlineSprite.setBounds(250 - 8, y - 30, backWidth, backHeight);
        } else if (activePlayer == player4) {
            outlineSprite.setBounds(355 - 8, y - 30, backWidth, backHeight);
        }
    }

    private void switchScore() {

        switch (playerNumber) {
            case TWO:
                chosenPlayerSprites.get(0).setScore(player1.getScore());
                chosenPlayerSprites.get(1).setScore(player2.getScore());
                break;
            case THREE:
                chosenPlayerSprites.get(0).setScore(player1.getScore());
                chosenPlayerSprites.get(1).setScore(player2.getScore());
                chosenPlayerSprites.get(2).setScore(player3.getScore());
                break;
            case FOUR:
                chosenPlayerSprites.get(0).setScore(player1.getScore());
                chosenPlayerSprites.get(1).setScore(player2.getScore());
                chosenPlayerSprites.get(2).setScore(player3.getScore());
                chosenPlayerSprites.get(3).setScore(player4.getScore());
                break;
        }
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0 / 255f, 0 / 255f, 0 / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();

        batch.draw(background, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        if (playerNumber != PlayerNumber.ONE) {
            renderPlayerSprites();
            switchScore();
            outlineSprite.draw(batch);
            if (totalScore() == cardArray.size / 2) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new WinningScreen(batch, camera, user, gMan, assetLoader, difficulty
                        , playerNumber, chosenPlayerSprites, background));
            }
        } else {
            if (cardsLeft == 0) {
                ((Game) Gdx.app.getApplicationListener()).setScreen(new WinningScreen(batch, camera, user, gMan, assetLoader, difficulty
                        , playerNumber, chosenPlayerSprites, background));
            }
        }

        cMan.renderCards(difficulty, batch);
        cardsDropped = cMan.getcDropper().isDroppedAll();

        for (Card card : cardArray) { // emptywood
            if (!card.isSolved()) {
                batch.draw(cardBackground, card.getSpriteRect().x, card.getSpriteRect().y + height, width, card.getBackgroundHeight());
            }
        }

        for (Card card : cardArray) {
            if (card.isChosen()) {
                card.setBackgroundHeight(card.getBackgroundHeight() + uncoverSpeed * delta);
                if (card.getBackgroundHeight() > 0) {
                    card.setBackgroundHeight(0);
                }
            } else if (card.isUp()) {
                card.setBackgroundHeight(card.getBackgroundHeight() - uncoverSpeed * delta);
                if (card.getBackgroundHeight() < -height) {
                    card.setBackgroundHeight(-height);
                    card.setUp(false);
                }
            }
        }

        checkIfEqual(delta);
        showCurrentPlayer();

        batch.end();
    }

    private void jumpPlayer1() {
        player2Jumped = false;
        player3Jumped = false;
        player4Jumped = false;

        if (!player1Jumped) {
            player1Y += jumpSpeed * Gdx.graphics.getDeltaTime();
        }
        if (player1Y > jumpCeiling) {
            player1Y = 715;
            player1Jumped = true;

        }

    }

    private void jumpPlayer2() {
        player1Jumped = false;
        player3Jumped = false;
        player4Jumped = false;

        if (!player2Jumped) {
            player2Y += jumpSpeed * Gdx.graphics.getDeltaTime();
        }
        if (player2Y > jumpCeiling) {
            player2Y = 715;
            player2Jumped = true;

        }

    }

    private void jumpPlayer3() {
        player1Jumped = false;
        player2Jumped = false;
        player4Jumped = false;

        if (!player3Jumped) {
            player3Y += jumpSpeed * Gdx.graphics.getDeltaTime();
        }
        if (player3Y > jumpCeiling) {
            player3Y = 715;
            player3Jumped = true;

        }

    }

    private void jumpPlayer4() {
        player1Jumped = false;
        player2Jumped = false;
        player3Jumped = false;

        if (!player4Jumped) {
            player4Y += jumpSpeed * Gdx.graphics.getDeltaTime();
        }
        if (player4Y > jumpCeiling) {
            player4Y = 715;
            player4Jumped = true;

        }

    }

    private void showCurrentPlayer() {

        float alpha = 0.3f;

        if (playerNumber == PlayerNumber.TWO) {

            if (activePlayer == player1) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(1);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                jumpPlayer1();

            } else if (activePlayer == player2) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(1);
                jumpPlayer2();
            }

        } else if (playerNumber == PlayerNumber.THREE) {

            if (activePlayer == player1) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(1);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(2).getSprite().setAlpha(alpha);
                jumpPlayer1();

            } else if (activePlayer == player2) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(1);
                chosenPlayerSprites.get(2).getSprite().setAlpha(alpha);
                jumpPlayer2();

            } else if (activePlayer == player3) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(2).getSprite().setAlpha(1);
                jumpPlayer3();
            }
        } else if (playerNumber == PlayerNumber.FOUR) {

            if (activePlayer == player1) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(1);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(2).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(3).getSprite().setAlpha(alpha);
                jumpPlayer1();

            } else if (activePlayer == player2) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(1);
                chosenPlayerSprites.get(2).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(3).getSprite().setAlpha(alpha);
                jumpPlayer2();

            } else if (activePlayer == player3) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(2).getSprite().setAlpha(1);
                chosenPlayerSprites.get(3).getSprite().setAlpha(alpha);
                jumpPlayer3();

            } else if (activePlayer == player4) {
                chosenPlayerSprites.get(0).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(1).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(2).getSprite().setAlpha(alpha);
                chosenPlayerSprites.get(3).getSprite().setAlpha(1);
                jumpPlayer4();
            }
        }
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        for (int i = 0; i < cardArray.size; i++) {
            if (cardsDropped && user.taps(cardArray.get(i).getGroundRect()) && !cardArray.get(i).isSolved()) {
                if (chosenCards.size < 2) {
                    cardArray.get(i).setChosen(true);
                    if (!chosenCards.contains(cardArray.get(i), true)) {
                        chosenCards.add(cardArray.get(i));
                    }
                }
            }
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        return true;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, user, gMan, assetLoader));
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();

    }
}