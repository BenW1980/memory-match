package game.peanutpanda.memorymatch.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;

import game.peanutpanda.memorymatch.gamedata.AssetLoader;
import game.peanutpanda.memorymatch.gamedata.Difficulty;
import game.peanutpanda.memorymatch.gamedata.GameManager;
import game.peanutpanda.memorymatch.gamedata.ScreenSize;
import game.peanutpanda.memorymatch.gamedata.User;
import game.peanutpanda.memorymatch.gameobjects.PlayerNumber;
import game.peanutpanda.memorymatch.gameobjects.PlayerSprite;

public class WinningScreen implements Screen, InputProcessor {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private User user;
    private GameManager gMan;
    private AssetLoader assetLoader;
    private Difficulty difficulty;
    private Texture background;
    private FitViewport viewport;
    private Rectangle menuButtonRect;
    private Rectangle refreshButtonRect;
    private Sprite menuButtonSprite;
    private Sprite refreshButtonSprite;
    private boolean refreshButtonTapped = false;
    private boolean menuButtonTapped = false;
    private Texture completedMessage;
    private Array<PlayerSprite> chosenPlayerSprites;
    private BitmapFont font;
    private PlayerNumber playerNumber;
    private Array<PlayerSprite> winningPlayerSprites;
    private int p1 = 0;
    private int p2 = 0;
    private int p3 = 0;
    private int p4 = 0;

    public WinningScreen(SpriteBatch batch, OrthographicCamera camera, User user, GameManager gMan, AssetLoader assetLoader, Difficulty difficulty
            , PlayerNumber playerNumber, Array<PlayerSprite> chosenPlayerSprites, Texture background) {

        Gdx.input.setInputProcessor(this);
        Gdx.input.setCatchBackKey(true);
        this.camera = camera;
        this.batch = batch;
        camera.setToOrtho(false, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());
        this.chosenPlayerSprites = chosenPlayerSprites;
        this.winningPlayerSprites = new Array<PlayerSprite>();
        winningPlayerSprites.addAll(chosenPlayerSprites);
        winningPlayerSprites.sort();
        this.user = user;
        this.gMan = gMan;
        this.assetLoader = assetLoader;
        this.difficulty = difficulty;
        this.playerNumber = playerNumber;
        this.background = background;
        viewport = new FitViewport(ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize(), camera);
        menuButtonSprite = new Sprite(assetLoader.menuButton());
        refreshButtonSprite = new Sprite(assetLoader.refreshButton());
        completedMessage = completedMessage();
        this.font = assetLoader.getAssetManager().get("size10.ttf", BitmapFont.class);

        switch (playerNumber) {

            case TWO:
                p1 = winningPlayerSprites.get(0).getScore();
                p2 = winningPlayerSprites.get(1).getScore();
                break;

            case THREE:
                p1 = winningPlayerSprites.get(0).getScore();
                p2 = winningPlayerSprites.get(1).getScore();
                p3 = winningPlayerSprites.get(2).getScore();
                break;

            case FOUR:
                p1 = winningPlayerSprites.get(0).getScore();
                p2 = winningPlayerSprites.get(1).getScore();
                p3 = winningPlayerSprites.get(2).getScore();
                p4 = winningPlayerSprites.get(3).getScore();
                break;

        }

        assetLoader.victory().play(0.6f);

    }

    @Override
    public void show() {

    }

    private void drawOneWinner() {
        batch.draw(winningPlayerSprites.get(0).getSprite(), ScreenSize.WIDTH.getSize() / 2 - 75, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
    }

    private void drawTwoWinners() {
        batch.draw(winningPlayerSprites.get(0).getSprite(), ScreenSize.WIDTH.getSize() / 2 - 170, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
        batch.draw(winningPlayerSprites.get(1).getSprite(), ScreenSize.WIDTH.getSize() / 2, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
    }

    private void drawThreeWinners() {
        batch.draw(winningPlayerSprites.get(0).getSprite(), 5, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
        batch.draw(winningPlayerSprites.get(1).getSprite(), 170, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
        batch.draw(winningPlayerSprites.get(2).getSprite(), 325, ScreenSize.HEIGHT.getSize() / 2 - 130, 150, 150);
    }

    private void drawFourWinners() {
        batch.draw(winningPlayerSprites.get(0).getSprite(), 5, ScreenSize.HEIGHT.getSize() / 2 - 130, 110, 110);
        batch.draw(winningPlayerSprites.get(1).getSprite(), 125, ScreenSize.HEIGHT.getSize() / 2 - 130, 110, 110);
        batch.draw(winningPlayerSprites.get(2).getSprite(), 240, ScreenSize.HEIGHT.getSize() / 2 - 130, 110, 110);
        batch.draw(winningPlayerSprites.get(3).getSprite(), 355, ScreenSize.HEIGHT.getSize() / 2 - 130, 110, 110);
    }

    private void renderPlayerSprites() {

        int width = 75;
        int height = 75;
        int backWidth = 90;
        int backHeight = 110;
        int y = 715;

        switch (playerNumber) {
            case TWO:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, y, width, height);
                font.draw(batch, "" + chosenPlayerSprites.get(0).getScore(), 74, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(1).getScore(), 179, y - 5);
                break;
            case THREE:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, y, width, height);
                chosenPlayerSprites.get(2).getSprite().setBounds(250, y, width, height);
                font.draw(batch, "" + chosenPlayerSprites.get(0).getScore(), 74, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(1).getScore(), 179, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(2).getScore(), 284, y - 5);
                break;
            case FOUR:
                batch.draw(assetLoader.playerBack(), 40 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 145 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 250 - 8, y - 30, backWidth, backHeight);
                batch.draw(assetLoader.playerBack(), 355 - 8, y - 30, backWidth, backHeight);
                chosenPlayerSprites.get(0).getSprite().setBounds(40, y, width, height);
                chosenPlayerSprites.get(1).getSprite().setBounds(145, y, width, height);
                chosenPlayerSprites.get(2).getSprite().setBounds(250, y, width, height);
                chosenPlayerSprites.get(3).getSprite().setBounds(355, y, width, height);
                font.draw(batch, "" + chosenPlayerSprites.get(0).getScore(), 74, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(1).getScore(), 179, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(2).getScore(), 284, y - 5);
                font.draw(batch, "" + chosenPlayerSprites.get(3).getScore(), 389, y - 5);
                break;
        }

        for (PlayerSprite ps : chosenPlayerSprites) {
            ps.getSprite().setAlpha(1);
            ps.getSprite().draw(batch);
        }
    }


    private Texture completedMessage() {

        int random = MathUtils.random(1, 4);
        Texture texture = null;

        switch (random) {

            case 1:
                texture = assetLoader.superduper();
                break;
            case 2:
                texture = assetLoader.awesome();
                break;
            case 3:
                texture = assetLoader.welldone();
                break;
            case 4:
                texture = assetLoader.nicejob();
                break;
        }

        return texture;
    }

    private void showLevelComplete() {

        batch.draw(completedMessage, ScreenSize.WIDTH.getSize() / 2 - 175, ScreenSize.HEIGHT.getSize() / 2 + 180, 350, 75);
        batch.draw(assetLoader.stars(), ScreenSize.WIDTH.getSize() / 2 - 130, ScreenSize.HEIGHT.getSize() / 2 + 30, 263, 140);
        drawLevelCompleteButtons();

        switch (playerNumber) {
            case TWO:
                if (p1 == p2) {
                    drawTwoWinners();
                } else {
                    drawOneWinner();
                }
                break;

            case THREE:
                if (p1 == p3) {
                    drawThreeWinners();
                } else if (p1 == p2) {
                    drawTwoWinners();
                } else {
                    drawOneWinner();
                }
                break;

            case FOUR:
                if (p1 == p4) {
                    drawFourWinners();
                } else if (p1 == p3) {
                    drawThreeWinners();
                } else if (p1 == p2) {
                    drawTwoWinners();
                } else {
                    drawOneWinner();
                }
                break;
        }
    }

    private void drawLevelCompleteButtons() {
        menuButtonRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 190, ScreenSize.HEIGHT.getSize() / 2 - 300, 180, 122);
        refreshButtonRect = new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 20, ScreenSize.HEIGHT.getSize() / 2 - 300, 180, 122);

        if (menuButtonTapped) {
            menuButtonSprite.setBounds(menuButtonRect.x, menuButtonRect.y, menuButtonRect.width - 5, menuButtonRect.height - 5);
        } else {
            menuButtonSprite.setBounds(menuButtonRect.x, menuButtonRect.y, menuButtonRect.width, menuButtonRect.height);
        }

        if (refreshButtonTapped) {
            refreshButtonSprite.setBounds(refreshButtonRect.x, refreshButtonRect.y, refreshButtonRect.width - 5, refreshButtonRect.height - 5);
        } else {
            refreshButtonSprite.setBounds(refreshButtonRect.x, refreshButtonRect.y, refreshButtonRect.width, refreshButtonRect.height);
        }

        menuButtonSprite.draw(batch);
        refreshButtonSprite.draw(batch);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(background, 0, 0, ScreenSize.WIDTH.getSize(), ScreenSize.HEIGHT.getSize());

        showLevelComplete();

        if (playerNumber != PlayerNumber.ONE) {
            renderPlayerSprites();
            showLevelComplete();
        }


        batch.end();


    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        if (user.taps(menuButtonRect)) {
            menuButtonTapped = true;
        }

        if (user.taps(refreshButtonRect)) {
            refreshButtonTapped = true;

        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (user.taps(menuButtonRect)) {
            menuButtonTapped = false;
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, user, gMan, assetLoader));
        }

        if (user.taps(refreshButtonRect)) {
            refreshButtonTapped = false;
            ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(batch, camera, user, gMan, assetLoader, difficulty, playerNumber, chosenPlayerSprites));

        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (pointer > 0) {
            return false;
        }

        Vector3 worldCoordinates = new Vector3(screenX, screenY, 0);
        camera.unproject(worldCoordinates, viewport.getScreenX(), viewport.getScreenY(), viewport.getScreenWidth(), viewport.getScreenHeight());

        user.move(worldCoordinates);

        menuButtonTapped = user.taps(menuButtonRect);
        refreshButtonTapped = user.taps(refreshButtonRect);

        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.BACK) {
            ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenuScreen(batch, camera, user, gMan, assetLoader));
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        assetLoader.getAssetManager().dispose();

    }

}

