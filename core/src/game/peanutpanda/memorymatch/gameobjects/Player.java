package game.peanutpanda.memorymatch.gameobjects;

public class Player {

    private int score;

    public Player() {
    }

    public void increaseScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


}
