package game.peanutpanda.memorymatch.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectSet;

import game.peanutpanda.memorymatch.gamedata.AssetLoader;
import game.peanutpanda.memorymatch.gamedata.Difficulty;
import game.peanutpanda.memorymatch.gamedata.ScreenSize;

public class CardManager {

    private AssetLoader assetLoader;
    private Array<Card> totalRegularCardsArray;
    private Array<Card> easyCardsArray;
    private Array<Card> mediumCardsArray;
    private Array<Card> hardCardsArray;
    private Array<Rectangle> cardRectanglesEasy;
    private Array<Rectangle> cardRectanglesMedium;
    private Array<Rectangle> cardRectanglesHard;
    private CardDropper cDropper;

    private int easyWidth = 135;
    private int easyHeight = 180;

    private int mediumWidth = 100;
    private int mediumHeight = 133;

    private int hardWidth = 80;
    private int hardHeight = 107;

    private float alphaSpeed;

    public CardManager(AssetLoader assetLoader) {

        this.assetLoader = assetLoader;

        this.totalRegularCardsArray = new Array<Card>();

        this.easyCardsArray = new Array<Card>();

        this.mediumCardsArray = new Array<Card>();
        this.hardCardsArray = new Array<Card>();

        cDropper = new CardDropper();

        alphaSpeed = Gdx.graphics.getDeltaTime() * 6;


    }

    public void fillTotalRegularCardsArray() { // vul array met 10 regular cards

        totalRegularCardsArray.add(new Card(1, assetLoader.animal1()));
        totalRegularCardsArray.add(new Card(2, assetLoader.animal2()));
        totalRegularCardsArray.add(new Card(3, assetLoader.animal3()));
        totalRegularCardsArray.add(new Card(4, assetLoader.animal4()));
        totalRegularCardsArray.add(new Card(5, assetLoader.animal5()));
        totalRegularCardsArray.add(new Card(6, assetLoader.animal6()));
        totalRegularCardsArray.add(new Card(7, assetLoader.animal7()));
        totalRegularCardsArray.add(new Card(8, assetLoader.animal8()));
        totalRegularCardsArray.add(new Card(9, assetLoader.animal9()));
        totalRegularCardsArray.add(new Card(10, assetLoader.animal10()));

    }

    public void generateCards(Difficulty difficulty) {

        switch (difficulty) {

            case EASY:
                generateEasyCards();
                break;
            case MEDIUM:
                generateMediumCards();
                break;
            case HARD:
                generateHardCards();
                break;
        }
    }

    public void renderCards(Difficulty difficulty, SpriteBatch batch) {

        switch (difficulty) {

            case EASY:
                cDropper.dropEasyCards(cardRectanglesEasy);
                renderEasyCards(batch);
                break;
            case MEDIUM:
                cDropper.dropMediumCards(cardRectanglesMedium);
                renderMediumCards(batch);
                break;
            case HARD:
                cDropper.dropHardCards(cardRectanglesHard);
                renderHardCards(batch);
                break;
        }

    }

    private void generateEasyCards() { // 2x3 random regular cards

        ObjectSet<Card> cardSet = new ObjectSet<Card>();

        while (cardSet.size < 3) {
            Card card = totalRegularCardsArray.random();
            cardSet.add(card);
        }

        for (Card card : cardSet) {
            Card card2 = copy(card);
            easyCardsArray.add(card);
            easyCardsArray.add(card2);
        }

        easyCardsArray.shuffle();
        createEasyRectangles();

    }

    private void createEasyRectangles() {

        cardRectanglesEasy = new Array<Rectangle>();

        for (int i = 0; i < 3; i++) {
            cardRectanglesEasy.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 145, ScreenSize.HEIGHT.getSize() + 100, easyWidth, easyHeight));
        }

        for (int i = 0; i < 3; i++) {
            cardRectanglesEasy.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2, ScreenSize.HEIGHT.getSize() + 100, easyWidth, easyHeight));
        }
    }

    private void createMediumRectangles() {

        cardRectanglesMedium = new Array<Rectangle>();

        for (int i = 0; i < 4; i++) {
            cardRectanglesMedium.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 165, ScreenSize.HEIGHT.getSize() + 100, mediumWidth, mediumHeight));
        }

        for (int i = 0; i < 4; i++) {
            cardRectanglesMedium.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 52, ScreenSize.HEIGHT.getSize() + 100, mediumWidth, mediumHeight));
        }

        for (int i = 0; i < 4; i++) {
            cardRectanglesMedium.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 63, ScreenSize.HEIGHT.getSize() + 100, mediumWidth, mediumHeight));
        }
    }

    private void createHardRectangles() {

        cardRectanglesHard = new Array<Rectangle>();

        for (int i = 0; i < 5; i++) {
            cardRectanglesHard.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 183, ScreenSize.HEIGHT.getSize() + 100, hardWidth, hardHeight));
        }

        for (int i = 0; i < 5; i++) {
            cardRectanglesHard.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 - 91, ScreenSize.HEIGHT.getSize() + 100, hardWidth, hardHeight));
        }

        for (int i = 0; i < 5; i++) {
            cardRectanglesHard.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 1, ScreenSize.HEIGHT.getSize() + 100, hardWidth, hardHeight));
        }

        for (int i = 0; i < 5; i++) {
            cardRectanglesHard.add(new Rectangle(ScreenSize.WIDTH.getSize() / 2 + 93, ScreenSize.HEIGHT.getSize() + 100, hardWidth, hardHeight));
        }

    }

    private void renderEasyCards(SpriteBatch batch) {

        for (int i = 0; i < easyCardsArray.size; i++) {

            easyCardsArray.get(i).getCardSprite().setBounds(easyCardsArray.get(i).getSpriteRect().x, easyCardsArray.get(i).getSpriteRect().y
                    , easyCardsArray.get(i).getSpriteRect().width, easyCardsArray.get(i).getSpriteRect().height);
            easyCardsArray.get(i).getCardSprite().draw(batch);

            if (easyCardsArray.get(i).isSolved() && easyCardsArray.get(i).getCardSprite().getColor().a != 0) {
                easyCardsArray.get(i).getCardSprite().setAlpha(easyCardsArray.get(i).getCardSprite().getColor().a - alphaSpeed);
                if (easyCardsArray.get(i).getCardSprite().getColor().a < 0.1f) {
                    easyCardsArray.get(i).getCardSprite().setAlpha(0);
                }
            }
        }

        for (int i = 0; i < cardRectanglesEasy.size; i++) {
            easyCardsArray.get(i).setSpriteRect(cardRectanglesEasy.get(i));
            easyCardsArray.get(i).getGroundRect().set(cardRectanglesEasy.get(i).x, cardRectanglesEasy.get(i).y, 150, 200);
        }

    }

    private void renderMediumCards(SpriteBatch batch) {

        for (int i = 0; i < mediumCardsArray.size; i++) {

            mediumCardsArray.get(i).getCardSprite().setBounds(mediumCardsArray.get(i).getSpriteRect().x, mediumCardsArray.get(i).getSpriteRect().y
                    , mediumCardsArray.get(i).getSpriteRect().width, mediumCardsArray.get(i).getSpriteRect().height);
            mediumCardsArray.get(i).getCardSprite().draw(batch);

            if (mediumCardsArray.get(i).isSolved() && mediumCardsArray.get(i).getCardSprite().getColor().a != 0) {
                mediumCardsArray.get(i).getCardSprite().setAlpha(mediumCardsArray.get(i).getCardSprite().getColor().a - alphaSpeed);
                if (mediumCardsArray.get(i).getCardSprite().getColor().a < 0.1f) {
                    mediumCardsArray.get(i).getCardSprite().setAlpha(0);
                }
            }
        }

        for (int i = 0; i < cardRectanglesMedium.size; i++) {
            mediumCardsArray.get(i).setSpriteRect(cardRectanglesMedium.get(i));
            mediumCardsArray.get(i).getGroundRect().set(cardRectanglesMedium.get(i).x, cardRectanglesMedium.get(i).y, 100, 133);
        }
    }

    private void renderHardCards(SpriteBatch batch) {

        for (int i = 0; i < hardCardsArray.size; i++) {

            hardCardsArray.get(i).getCardSprite().setBounds(hardCardsArray.get(i).getSpriteRect().x, hardCardsArray.get(i).getSpriteRect().y
                    , hardCardsArray.get(i).getSpriteRect().width, hardCardsArray.get(i).getSpriteRect().height);
            hardCardsArray.get(i).getCardSprite().draw(batch);

            if (hardCardsArray.get(i).isSolved() && hardCardsArray.get(i).getCardSprite().getColor().a != 0) {
                hardCardsArray.get(i).getCardSprite().setAlpha(hardCardsArray.get(i).getCardSprite().getColor().a - alphaSpeed);
                if (hardCardsArray.get(i).getCardSprite().getColor().a < 0.1f) {
                    hardCardsArray.get(i).getCardSprite().setAlpha(0);
                }
            }
        }

        for (int i = 0; i < cardRectanglesHard.size; i++) {
            hardCardsArray.get(i).setSpriteRect(cardRectanglesHard.get(i));
            hardCardsArray.get(i).getGroundRect().set(cardRectanglesHard.get(i).x, cardRectanglesHard.get(i).y, 80, 107);
        }
    }


    private void generateMediumCards() { // 2x6 random regular cards

        ObjectSet<Card> cardSet = new ObjectSet<Card>();

        while (cardSet.size < 6) {
            Card card = totalRegularCardsArray.random();
            cardSet.add(card);
        }

        for (Card card : cardSet) {
            Card card2 = copy(card);
            mediumCardsArray.add(card);
            mediumCardsArray.add(card2);

        }

        mediumCardsArray.shuffle();
        createMediumRectangles();
    }


    private void generateHardCards() { // 2x10 random regular cards

        ObjectSet<Card> cardSet = new ObjectSet<Card>();

        while (cardSet.size < 10) {
            Card card = totalRegularCardsArray.random();
            cardSet.add(card);
        }

        for (Card card : cardSet) {
            Card card2 = copy(card);
            hardCardsArray.add(card);
            hardCardsArray.add(card2);
        }

        hardCardsArray.shuffle();
        createHardRectangles();
    }

    private Card copy(Card card) {

        return new Card(card.getNumber(), card.getCardTexture());

    }

    public void resetArrays() {

        totalRegularCardsArray = new Array<Card>();

        easyCardsArray = new Array<Card>();
        mediumCardsArray = new Array<Card>();
        hardCardsArray = new Array<Card>();

        cardRectanglesEasy = new Array<Rectangle>();
        cardRectanglesMedium = new Array<Rectangle>();
        cardRectanglesHard = new Array<Rectangle>();


    }

    public Array<Card> getEasyCardsArray() {
        return easyCardsArray;
    }

    public Array<Card> getMediumCardsArray() {
        return mediumCardsArray;
    }

    public Array<Card> getHardCardsArray() {
        return hardCardsArray;
    }

    public Array<Rectangle> getCardRectanglesEasy() {
        return cardRectanglesEasy;
    }

    public Array<Rectangle> getCardRectanglesMedium() {
        return cardRectanglesMedium;
    }

    public Array<Rectangle> getCardRectanglesHard() {
        return cardRectanglesHard;
    }

    public CardDropper getcDropper() {
        return cDropper;
    }

    public int getEasyWidth() {
        return easyWidth;
    }

    public int getEasyHeight() {
        return easyHeight;
    }

    public int getMediumWidth() {
        return mediumWidth;
    }

    public int getMediumHeight() {
        return mediumHeight;
    }

    public int getHardWidth() {
        return hardWidth;
    }

    public int getHardHeight() {
        return hardHeight;
    }

}
