package game.peanutpanda.memorymatch.gameobjects;

public enum PlayerNumber {

    ONE, TWO, THREE, FOUR;
}
