package game.peanutpanda.memorymatch.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class PlayerSprite implements Comparable<PlayerSprite> {

    private Sprite sprite;
    private Rectangle rect;
    private boolean chosen;
    private int score;

    public PlayerSprite(Texture texture, Rectangle rect, boolean chosen) {

        sprite = new Sprite(texture);
        this.rect = rect;
        this.chosen = chosen;
        sprite.setBounds(rect.x, rect.y, rect.width, rect.height);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public Rectangle getRect() {
        return rect;
    }

    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public int compareTo(PlayerSprite comparePlayer) {

        int compareScore = comparePlayer.getScore();

        return compareScore - this.score;
    }
}
