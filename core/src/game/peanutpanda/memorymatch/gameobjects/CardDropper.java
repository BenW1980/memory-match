package game.peanutpanda.memorymatch.gameobjects;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

import game.peanutpanda.memorymatch.gamedata.ScreenSize;

public class CardDropper {

    private boolean droppedAll = false;

    public void dropEasyCards(Array<Rectangle> cardRectanglesEasy) {

        float progress = 0.2f;
        boolean dropped1 = false;
        boolean dropped2 = false;
        boolean dropped3 = false;
        boolean dropped4 = false;
        boolean dropped5 = false;

        int row1 = 700;
        int row2 = 510;
        int row3 = 320;

        droppedAll = false;

        cardRectanglesEasy.get(0).y = MathUtils.lerp(cardRectanglesEasy.get(0).y, ScreenSize.HEIGHT.getSize() - row1, progress);
        if (cardRectanglesEasy.get(0).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
            dropped1 = true;
        }
        if (cardRectanglesEasy.get(0).y < ScreenSize.HEIGHT.getSize() - row1) {
            cardRectanglesEasy.get(0).y = ScreenSize.HEIGHT.getSize() - row1;
        }
        //-------------------------
        if (dropped1) {
            cardRectanglesEasy.get(3).y = MathUtils.lerp(cardRectanglesEasy.get(3).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesEasy.get(3).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped2 = true;
            }
            if (cardRectanglesEasy.get(3).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesEasy.get(3).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }

        //-------------------------
        if (dropped2) {
            cardRectanglesEasy.get(1).y = MathUtils.lerp(cardRectanglesEasy.get(1).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesEasy.get(1).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped3 = true;
            }
            if (cardRectanglesEasy.get(1).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesEasy.get(1).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }

        //-------------------------
        if (dropped3) {
            cardRectanglesEasy.get(4).y = MathUtils.lerp(cardRectanglesEasy.get(4).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesEasy.get(4).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped4 = true;
            }
            if (cardRectanglesEasy.get(4).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesEasy.get(4).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }

        //-------------------------
        if (dropped4) {
            cardRectanglesEasy.get(2).y = MathUtils.lerp(cardRectanglesEasy.get(2).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesEasy.get(2).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped5 = true;
            }
            if (cardRectanglesEasy.get(2).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesEasy.get(2).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }

        //-------------------------
        if (dropped5) {
            droppedAll = true;
            cardRectanglesEasy.get(5).y = MathUtils.lerp(cardRectanglesEasy.get(5).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesEasy.get(5).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesEasy.get(5).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }

    }

    public void dropMediumCards(Array<Rectangle> cardRectanglesMedium) {

        float progress = 0.2f;
        boolean dropped1 = false;
        boolean dropped2 = false;
        boolean dropped3 = false;
        boolean dropped4 = false;
        boolean dropped5 = false;
        boolean dropped6 = false;
        boolean dropped7 = false;
        boolean dropped8 = false;
        boolean dropped9 = false;
        boolean dropped10 = false;
        boolean dropped11 = false;

        droppedAll = false;

        int row1 = 700;
        int row2 = 560;
        int row3 = 420;
        int row4 = 280;

        cardRectanglesMedium.get(0).y = MathUtils.lerp(cardRectanglesMedium.get(0).y, ScreenSize.HEIGHT.getSize() - row1, progress);
        if (cardRectanglesMedium.get(0).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
            dropped1 = true;
        }
        if (cardRectanglesMedium.get(0).y < ScreenSize.HEIGHT.getSize() - row1) {
            cardRectanglesMedium.get(0).y = ScreenSize.HEIGHT.getSize() - row1;
        }
        //-------------------------
        if (dropped1) {
            cardRectanglesMedium.get(4).y = MathUtils.lerp(cardRectanglesMedium.get(4).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesMedium.get(4).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped2 = true;
            }
            if (cardRectanglesMedium.get(4).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesMedium.get(4).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }
        //-------------------------
        if (dropped2) {
            cardRectanglesMedium.get(8).y = MathUtils.lerp(cardRectanglesMedium.get(8).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesMedium.get(8).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped3 = true;
            }
            if (cardRectanglesMedium.get(8).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesMedium.get(8).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }
        //-------------------------
        if (dropped3) {
            cardRectanglesMedium.get(1).y = MathUtils.lerp(cardRectanglesMedium.get(1).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesMedium.get(1).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped4 = true;
            }
            if (cardRectanglesMedium.get(1).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesMedium.get(1).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped4) {
            cardRectanglesMedium.get(5).y = MathUtils.lerp(cardRectanglesMedium.get(5).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesMedium.get(5).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped5 = true;
            }
            if (cardRectanglesMedium.get(5).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesMedium.get(5).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped5) {
            cardRectanglesMedium.get(9).y = MathUtils.lerp(cardRectanglesMedium.get(9).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesMedium.get(9).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped6 = true;
            }
            if (cardRectanglesMedium.get(9).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesMedium.get(9).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped6) {
            cardRectanglesMedium.get(2).y = MathUtils.lerp(cardRectanglesMedium.get(2).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesMedium.get(2).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped7 = true;
            }
            if (cardRectanglesMedium.get(2).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesMedium.get(2).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped7) {
            cardRectanglesMedium.get(6).y = MathUtils.lerp(cardRectanglesMedium.get(6).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesMedium.get(6).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped8 = true;
            }
            if (cardRectanglesMedium.get(6).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesMedium.get(6).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped8) {
            cardRectanglesMedium.get(10).y = MathUtils.lerp(cardRectanglesMedium.get(10).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesMedium.get(10).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped9 = true;
            }
            if (cardRectanglesMedium.get(10).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesMedium.get(10).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped9) {
            cardRectanglesMedium.get(3).y = MathUtils.lerp(cardRectanglesMedium.get(3).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesMedium.get(3).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped10 = true;
            }
            if (cardRectanglesMedium.get(3).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesMedium.get(3).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped10) {
            cardRectanglesMedium.get(7).y = MathUtils.lerp(cardRectanglesMedium.get(7).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesMedium.get(7).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped11 = true;
            }
            if (cardRectanglesMedium.get(7).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesMedium.get(7).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped11) {
            droppedAll = true;
            cardRectanglesMedium.get(11).y = MathUtils.lerp(cardRectanglesMedium.get(11).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesMedium.get(11).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesMedium.get(11).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }


    }

    public void dropHardCards(Array<Rectangle> cardRectanglesHard) {

        float progress = 0.2f;
        boolean dropped1 = false;
        boolean dropped2 = false;
        boolean dropped3 = false;
        boolean dropped4 = false;
        boolean dropped5 = false;
        boolean dropped6 = false;
        boolean dropped7 = false;
        boolean dropped8 = false;
        boolean dropped9 = false;
        boolean dropped10 = false;
        boolean dropped11 = false;
        boolean dropped12 = false;
        boolean dropped13 = false;
        boolean dropped14 = false;
        boolean dropped15 = false;
        boolean dropped16 = false;
        boolean dropped17 = false;
        boolean dropped18 = false;
        boolean dropped19 = false;

        droppedAll = false;

        int row1 = 700;
        int row2 = 585;
        int row3 = 470;
        int row4 = 355;
        int row5 = 240;


        cardRectanglesHard.get(0).y = MathUtils.lerp(cardRectanglesHard.get(0).y, ScreenSize.HEIGHT.getSize() - row1, progress);
        if (cardRectanglesHard.get(0).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
            dropped1 = true;
        }
        if (cardRectanglesHard.get(0).y < ScreenSize.HEIGHT.getSize() - row1) {
            cardRectanglesHard.get(0).y = ScreenSize.HEIGHT.getSize() - row1;
        }
        //-------------------------
        if (dropped1) {
            cardRectanglesHard.get(5).y = MathUtils.lerp(cardRectanglesHard.get(5).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesHard.get(5).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped2 = true;
            }
            if (cardRectanglesHard.get(5).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesHard.get(5).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }
        //-------------------------
        if (dropped2) {
            cardRectanglesHard.get(10).y = MathUtils.lerp(cardRectanglesHard.get(10).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesHard.get(10).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped3 = true;
            }
            if (cardRectanglesHard.get(10).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesHard.get(10).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }
        //-------------------------
        if (dropped3) {
            cardRectanglesHard.get(15).y = MathUtils.lerp(cardRectanglesHard.get(15).y, ScreenSize.HEIGHT.getSize() - row1, progress);
            if (cardRectanglesHard.get(15).y < ScreenSize.HEIGHT.getSize() - row1 / 2) {
                dropped4 = true;
            }
            if (cardRectanglesHard.get(15).y < ScreenSize.HEIGHT.getSize() - row1) {
                cardRectanglesHard.get(15).y = ScreenSize.HEIGHT.getSize() - row1;
            }
        }
        //-------------------------
        if (dropped4) {
            cardRectanglesHard.get(1).y = MathUtils.lerp(cardRectanglesHard.get(1).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesHard.get(1).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped5 = true;
            }
            if (cardRectanglesHard.get(1).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesHard.get(1).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped5) {
            cardRectanglesHard.get(6).y = MathUtils.lerp(cardRectanglesHard.get(6).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesHard.get(6).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped6 = true;
            }
            if (cardRectanglesHard.get(6).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesHard.get(6).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped6) {
            cardRectanglesHard.get(11).y = MathUtils.lerp(cardRectanglesHard.get(11).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesHard.get(11).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped7 = true;
            }
            if (cardRectanglesHard.get(11).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesHard.get(11).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped7) {
            cardRectanglesHard.get(16).y = MathUtils.lerp(cardRectanglesHard.get(16).y, ScreenSize.HEIGHT.getSize() - row2, progress);
            if (cardRectanglesHard.get(16).y < ScreenSize.HEIGHT.getSize() - row2 / 2) {
                dropped8 = true;
            }
            if (cardRectanglesHard.get(16).y < ScreenSize.HEIGHT.getSize() - row2) {
                cardRectanglesHard.get(16).y = ScreenSize.HEIGHT.getSize() - row2;
            }
        }
        //-------------------------
        if (dropped8) {
            cardRectanglesHard.get(2).y = MathUtils.lerp(cardRectanglesHard.get(2).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesHard.get(2).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped9 = true;
            }
            if (cardRectanglesHard.get(2).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesHard.get(2).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped9) {
            cardRectanglesHard.get(7).y = MathUtils.lerp(cardRectanglesHard.get(7).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesHard.get(7).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped10 = true;
            }
            if (cardRectanglesHard.get(7).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesHard.get(7).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped10) {
            cardRectanglesHard.get(12).y = MathUtils.lerp(cardRectanglesHard.get(12).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesHard.get(12).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped11 = true;
            }
            if (cardRectanglesHard.get(12).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesHard.get(12).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped11) {
            cardRectanglesHard.get(17).y = MathUtils.lerp(cardRectanglesHard.get(17).y, ScreenSize.HEIGHT.getSize() - row3, progress);
            if (cardRectanglesHard.get(17).y < ScreenSize.HEIGHT.getSize() - row3 / 2) {
                dropped12 = true;
            }
            if (cardRectanglesHard.get(17).y < ScreenSize.HEIGHT.getSize() - row3) {
                cardRectanglesHard.get(17).y = ScreenSize.HEIGHT.getSize() - row3;
            }
        }
        //-------------------------
        if (dropped12) {
            cardRectanglesHard.get(3).y = MathUtils.lerp(cardRectanglesHard.get(3).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesHard.get(3).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped13 = true;
            }
            if (cardRectanglesHard.get(3).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesHard.get(3).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped13) {
            cardRectanglesHard.get(8).y = MathUtils.lerp(cardRectanglesHard.get(8).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesHard.get(8).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped14 = true;
            }
            if (cardRectanglesHard.get(8).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesHard.get(8).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped14) {
            cardRectanglesHard.get(13).y = MathUtils.lerp(cardRectanglesHard.get(13).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesHard.get(13).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped15 = true;
            }
            if (cardRectanglesHard.get(13).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesHard.get(13).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped15) {
            cardRectanglesHard.get(18).y = MathUtils.lerp(cardRectanglesHard.get(18).y, ScreenSize.HEIGHT.getSize() - row4, progress);
            if (cardRectanglesHard.get(18).y < ScreenSize.HEIGHT.getSize() - row4 / 2) {
                dropped16 = true;
            }
            if (cardRectanglesHard.get(18).y < ScreenSize.HEIGHT.getSize() - row4) {
                cardRectanglesHard.get(18).y = ScreenSize.HEIGHT.getSize() - row4;
            }
        }
        //-------------------------
        if (dropped16) {
            cardRectanglesHard.get(4).y = MathUtils.lerp(cardRectanglesHard.get(4).y, ScreenSize.HEIGHT.getSize() - row5, progress);
            if (cardRectanglesHard.get(4).y < ScreenSize.HEIGHT.getSize() - row5 / 2) {
                dropped17 = true;
            }
            if (cardRectanglesHard.get(4).y < ScreenSize.HEIGHT.getSize() - row5) {
                cardRectanglesHard.get(4).y = ScreenSize.HEIGHT.getSize() - row5;
            }
        }
        //-------------------------
        if (dropped17) {
            cardRectanglesHard.get(9).y = MathUtils.lerp(cardRectanglesHard.get(9).y, ScreenSize.HEIGHT.getSize() - row5, progress);
            if (cardRectanglesHard.get(9).y < ScreenSize.HEIGHT.getSize() - row5 / 2) {
                dropped18 = true;
            }
            if (cardRectanglesHard.get(9).y < ScreenSize.HEIGHT.getSize() - row5) {
                cardRectanglesHard.get(9).y = ScreenSize.HEIGHT.getSize() - row5;
            }
        }
        //-------------------------
        if (dropped18) {
            cardRectanglesHard.get(14).y = MathUtils.lerp(cardRectanglesHard.get(14).y, ScreenSize.HEIGHT.getSize() - row5, progress);
            if (cardRectanglesHard.get(14).y < ScreenSize.HEIGHT.getSize() - row5 / 2) {
                dropped19 = true;
            }
            if (cardRectanglesHard.get(14).y < ScreenSize.HEIGHT.getSize() - row5) {
                cardRectanglesHard.get(14).y = ScreenSize.HEIGHT.getSize() - row5;
            }
        }
        //-------------------------
        if (dropped19) {
            droppedAll = true;
            cardRectanglesHard.get(19).y = MathUtils.lerp(cardRectanglesHard.get(19).y, ScreenSize.HEIGHT.getSize() - row5, progress);
            if (cardRectanglesHard.get(19).y < ScreenSize.HEIGHT.getSize() - row5) {
                cardRectanglesHard.get(19).y = ScreenSize.HEIGHT.getSize() - row5;
            }
        }
    }

    public boolean isDroppedAll() {
        return droppedAll;
    }

}
