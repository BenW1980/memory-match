package game.peanutpanda.memorymatch.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Card {

    private boolean chosen = false;
    private boolean solved = false;
    private boolean up = false;
    private Texture cardTexture;
    private Sprite cardSprite;
    private int number;
    private Rectangle spriteRect;
    private Rectangle groundRect;
    private float backgroundHeight;

    public Card(int number, Texture cardTexture) {

        this.number = number;
        this.cardTexture = cardTexture;
        this.cardSprite = new Sprite(cardTexture);
        this.spriteRect = new Rectangle();
        this.groundRect = new Rectangle();

    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public int getNumber() {
        return number;
    }

    public Sprite getCardSprite() {
        return cardSprite;
    }

    public Rectangle getSpriteRect() {
        return spriteRect;
    }

    public void setSpriteRect(Rectangle spriteRect) {
        this.spriteRect = spriteRect;
    }

    public Texture getCardTexture() {
        return cardTexture;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public Rectangle getGroundRect() {
        return groundRect;
    }

    public boolean isUp() {
        return up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public float getBackgroundHeight() {
        return backgroundHeight;
    }

    public void setBackgroundHeight(float backgroundHeight) {
        this.backgroundHeight = backgroundHeight;
    }

}
