package game.peanutpanda.memorymatch;

import com.badlogic.gdx.Game;

import game.peanutpanda.memorymatch.gamedata.AdsController;
import game.peanutpanda.memorymatch.screens.LoadingScreen;

public class MemoryMatch extends Game {

    private AdsController adsController;

    public MemoryMatch(AdsController adsController) {
        this.adsController = adsController;
    }

    @Override
    public void create() {
        this.setScreen(new LoadingScreen(adsController));

    }

    @Override
    public void render() {
        super.render();
    }
}
