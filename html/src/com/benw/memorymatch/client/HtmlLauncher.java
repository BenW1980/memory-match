package com.benw.memorymatch.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

import game.peanutpanda.memorymatch.MemoryMatch;
import game.peanutpanda.memorymatch.gamedata.AdsController;

public class HtmlLauncher extends GwtApplication {

    AdsController ads;

    @Override
    public GwtApplicationConfiguration getConfig() {
        return new GwtApplicationConfiguration(480, 320);
    }

    @Override
    public ApplicationListener createApplicationListener() {
        return new MemoryMatch(ads);
    }
}